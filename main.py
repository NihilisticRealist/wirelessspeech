from bottle import route, run, request, redirect
from os import system

@route('/')
def index():
    return ('''
    <style>
        html {
            background-color: black;
            @import url('https://fonts.googleapis.com/css?family=Chilanka&display=swap');
            font-family: 'Chilanka', cursive;
            scolor: white;
        }
        #title {
            color: white;
            align: center;
        }
        
    </style>
    <h1 id="title">doggobot uwu</h1>
    <form action="/speak" method="post">
        what to say: <input name="username" type="text" />
        <input value="Login" type="submit" />
    </form>
    
    ''')

@route('/speak', method="POST")
def speak():
    text = request.forms.get('username')
    system('espeak "'+text+'"')
    redirect('/')

run(host='0.0.0.0', port=80, debug=True)